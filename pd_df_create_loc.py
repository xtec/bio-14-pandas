import pandas as pd

pets = {
    'species': ['cat', 'dog', 'parrot', 'cockroach'], 
    'name': ['Dr. Mittens Lamar', 'Diesel', 'Peach', 'Richard'], 
    'legs': [4, 4, 2, 6],
    'wings': [0, 0, 2, 4],
    'looking_for_home': ['no', 'no', 'no', 'yes']
}
df = pd.DataFrame(pets)

# Rename columns
df.rename(columns={'name': 'pet_name', 'looking_for_home': 'homeless'}, inplace=True)
print(df.head())

people = {
    "first_name": ["Michael", "Michael", 'Jane', 'John'], 
    "last_name": ["Jackson", "Jordan", 'Doe', 'Doe'], 
    "email": ["mjackson@email.com", "mjordan@email.com", 
'JaneDoe@email.com', 'JohnDoe@email.com'],
    "birthday": ["29.09.1958", "17.02.1963", "15.03.1978", "12.05.1979"],
    "height": [1.75, 1.98, 1.64, 1.8]
}
df2 = pd.DataFrame(people)

# Define index
df.index = ['first', 'second', 'third', 'fourth']
df.index.name = 'index'
df.head()

# Queries with loc.
print("--- Test loc method -----")
print(df2.loc[['first','fourth']])
print(df2.loc[['first','fourth'], ['last_name', 'birthday']])

# Filter with conditionals.
print('Menors de 1.8')
print(df[df.height < 1.8])

# Reset index.
df.reset_index(drop=True, inplace=True)
df.head()

# Queries with iloc.
print("--- Test iloc method -----")
print(df.iloc[[1, 2], [1, 2]])