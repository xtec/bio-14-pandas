import pandas as pd

# Source file: https://raw.githubusercontent.com/mwaskom/seaborn-data/master/penguins.csv
df = pd.read_csv('./data/penguins.csv')

df.head(3)

# Example 1. Mitjana del pes de tots.
print(df.body_mass_g.agg('median'))

# Example 2. Recompte de valors NaN
def count_nulls(series, ok_message=0):
    if not series.isna().sum():
        return ok_message
    return len(series) - series.count()

print(df.agg(count_nulls, ok_message='Hurray!'))

# Exemple 3. Tamany mitjà de les aletes de cada gènere.
df.groupby(['sex']).agg({'bill_length_mm':'median'})

# Exemple 4. Tamany mitjà de les aletes de cada gènere i exemplars de cada illa.
print(df.groupby(['island', 'sex']).agg({'bill_length_mm':'median'}))