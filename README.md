# Pandas

## Our tutorials

[Bioinformatics -> Pandas. xtec.dev](https://xtec.dev/python/pandas/)

## Getting started

Install virtual environment

```sh
$ sudo apt install python3-venv
```

Activate `venv` and install dependencies

```sh
$ python3 -m venv .venv
$ source .venv/bin/activate
(.venv) $ python3 -m pip install --upgrade pip
(.venv) $ pip install -r requirements-dev.txt
```

## Exemples de Pandas nivell inicial.

**pd_df_create_loc.py**

Crea un dataFrame nou amb Python i prova alguns dels mètodes més habituals i potents a Pandas. 

**pd_df_load_file.py**

Pandas permet carregar dades d'un CSV (el format més habitual) tant si està descarregat com si està publicat en una web. El que no sap tanta gent és com fer-ho per descarregar el fitxer CSV al nostre disc només si no el tenim, si el tenim l'obrirà del nostre disc (estalviant així energia, l'accés a un fitxer que ja tenim descarregat consumeix menys que si hem d'accedir a un servidor web llunyà)

**pd_df_Xgroup_agg.py**

Exemples de funcions d'agrupació i agregació, que Pandas resolt molt eficaçment i són molt útils.

## Datasets interessants.

A la carpeta data n'hi ha uns quants. 

Són tots provinents de portals de dades obertes.


