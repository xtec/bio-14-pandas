from urllib.request import urlretrieve
import os.path
import pandas as pd

url: str = "https://gitlab.com/xtec/bio-pandas/-/raw/main/data/cars.csv"
csv_file: str = "cars.csv"

# Descarreguem el fitxer si no el tenim al directori
if not os.path.isfile(csv_file):
   print(f"Download fitxer {csv_file}")
   urlretrieve(url, csv_file)

df_cars: pd.DataFrame = pd.read_csv(csv_file, sep=',')

# Informació important del dataFrame.
print(df_cars.info())
print(df_cars.head())
print(df_cars.describe())